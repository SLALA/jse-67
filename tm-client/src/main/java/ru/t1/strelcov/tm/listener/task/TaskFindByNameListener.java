package ru.t1.strelcov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskFindByNameRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class TaskFindByNameListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-find-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Find task by name.";
    }

    @Override
    @EventListener(condition = "@taskFindByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[FIND TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.findByNameTask(new TaskFindByNameRequest(getToken(), name)).getTask();
        showTask(task);
    }

}
