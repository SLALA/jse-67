package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class UserRemoveByLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserRemoveByLoginRequest(@Nullable final String token, @Nullable final String login) {
        super(token);
        this.login = login;
    }

}
