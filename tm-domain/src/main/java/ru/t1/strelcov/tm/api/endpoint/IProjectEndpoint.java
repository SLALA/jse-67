package ru.t1.strelcov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectEndpoint {

    @NotNull String SPACE = "http://endpoint.tm.strelcov.t1.ru/";
    @NotNull String PART = "ProjectEndpointService";
    @NotNull String NAME = "ProjectEndpoint";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdlURL);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IProjectEndpoint.class);
    }

    @WebMethod
    @NotNull
    ProjectListResponse listProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListRequest request);

    @WebMethod
    @NotNull
    ProjectListSortedResponse listSortedProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListSortedRequest request);

    @WebMethod
    @NotNull
    ProjectChangeStatusByIdResponse changeStatusByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIdRequest request);

    @WebMethod
    @NotNull
    ProjectChangeStatusByNameResponse changeStatusByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByNameRequest request);

    @WebMethod
    @NotNull
    ProjectClearResponse clearProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectClearRequest request);

    @WebMethod
    @NotNull
    ProjectCreateResponse createProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectCreateRequest request);

    @WebMethod
    @NotNull
    ProjectFindByIdResponse findByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectFindByIdRequest request);

    @WebMethod
    @NotNull
    ProjectFindByNameResponse findByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectFindByNameRequest request);

    @WebMethod
    @NotNull
    ProjectRemoveByIdResponse removeByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request);

    @WebMethod
    @NotNull
    ProjectRemoveByNameResponse removeByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByNameRequest request);

    @WebMethod
    @NotNull
    ProjectRemoveWithTasksResponse removeWithTasksProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveWithTasksRequest request);

    @WebMethod
    @NotNull
    ProjectUpdateByIdResponse updateByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIdRequest request);

    @WebMethod
    @NotNull
    ProjectUpdateByNameResponse updateByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByNameRequest request);

}
