package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.UserDTO;

@Setter
@Getter
@NoArgsConstructor
public final class UserUpdateByLoginResponse extends AbstractUserResponse {

    public UserUpdateByLoginResponse(@NotNull final UserDTO user) {
        super(user);
    }

}
