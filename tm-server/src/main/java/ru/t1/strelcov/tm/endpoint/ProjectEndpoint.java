package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.api.service.dto.IProjectDTOService;
import ru.t1.strelcov.tm.dto.model.SessionDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Status;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint")
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Autowired
    @NotNull
    private IProjectDTOService projectDTOService;

    @NotNull
    @Override
    public ProjectListResponse listProject(@NotNull ProjectListRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectListResponse(projectDTOService.findAll(session.getUserId()));
    }

    @NotNull
    @Override
    public ProjectListSortedResponse listSortedProject(@NotNull ProjectListSortedRequest request) {
        @NotNull final SessionDTO session = check(request);
        final String sort = request.getSort();
        return new ProjectListSortedResponse(projectDTOService.findAll(session.getUserId(), sort));
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusByIdProject(@NotNull ProjectChangeStatusByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectChangeStatusByIdResponse(projectDTOService.changeStatusById(session.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public ProjectChangeStatusByNameResponse changeStatusByNameProject(@NotNull ProjectChangeStatusByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectChangeStatusByNameResponse(projectDTOService.changeStatusByName(session.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) {
        @NotNull final SessionDTO session = check(request);
        projectDTOService.clear(session.getUserId());
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectCreateResponse(projectDTOService.add(session.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public ProjectFindByIdResponse findByIdProject(@NotNull ProjectFindByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectFindByIdResponse(projectDTOService.findById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public ProjectFindByNameResponse findByNameProject(@NotNull ProjectFindByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectFindByNameResponse(projectDTOService.findByName(session.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeByIdProject(@NotNull ProjectRemoveByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectRemoveByIdResponse(projectDTOService.removeById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public ProjectRemoveByNameResponse removeByNameProject(@NotNull ProjectRemoveByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectRemoveByNameResponse(projectDTOService.removeByName(session.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public ProjectRemoveWithTasksResponse removeWithTasksProject(@NotNull ProjectRemoveWithTasksRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectRemoveWithTasksResponse(projectDTOService.removeProjectWithTasksById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateByIdProject(@NotNull ProjectUpdateByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectUpdateByIdResponse(projectDTOService.updateById(session.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public ProjectUpdateByNameResponse updateByNameProject(@NotNull ProjectUpdateByNameRequest request) {
        @NotNull final SessionDTO session = check(request);
        return new ProjectUpdateByNameResponse(projectDTOService.updateByName(session.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }
}
