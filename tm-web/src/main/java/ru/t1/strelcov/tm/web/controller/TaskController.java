package ru.t1.strelcov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.strelcov.tm.web.api.service.IProjectService;
import ru.t1.strelcov.tm.web.api.service.ITaskService;
import ru.t1.strelcov.tm.web.enumerated.Status;
import ru.t1.strelcov.tm.web.model.Task;

@Controller
@RequestMapping("/task")
public class TaskController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;

    @GetMapping("/list")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", taskService.findAll());
    }

    @PostMapping("/create")
    public String add() {
        int number = taskService.findAll().size() + 1;
        taskService.add("Task " + number, "Task " + number);
        return "redirect:/task/list";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(
            @NotNull @PathVariable("id") final String id) {
        @Nullable final Task task = taskService.findById(id);
        if (task == null) return list();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("task") final Task task,
            @NotNull final BindingResult result
    ) {
        taskService.add(task);
        return "redirect:/task/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(
            @NotNull @PathVariable("id") final String id) {
        taskService.deleteById(id);
        return "redirect:/task/list";
    }

}
