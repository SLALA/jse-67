package ru.t1.strelcov.tm.web.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.strelcov.tm.web.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {
}
