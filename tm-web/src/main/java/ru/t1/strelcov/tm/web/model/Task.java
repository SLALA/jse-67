package ru.t1.strelcov.tm.web.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.strelcov.tm.web.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import static ru.t1.strelcov.tm.web.enumerated.Status.NOT_STARTED;

@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_task")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Task {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column(nullable = false)
    @NotNull
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull
    private Status status = NOT_STARTED;

    @Column(nullable = false)
    @NotNull
    private Date created = new Date();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @Nullable
    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date dateStart;

    public Task(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public Task(@NotNull String name, @Nullable String description, @Nullable String projectId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
    }

    public Task(@NotNull String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id.equals(task.id) && name.equals(task.name) && status == task.status && created.equals(task.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, projectId, status, created);
    }

}
